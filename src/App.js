import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Components
import Header from './components/Header';
import MainBackground from './components/MainBackground';

// Pages
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import ProjectsPage from './pages/ProjectsPage';
import GalleryPage from './pages/GalleryPage';
import ContactPage from './pages/ContactPage';

// Includes
import './Assets/css/app.min.css';
import 'react-perfect-scrollbar/dist/css/styles.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <MainBackground />
          <Header />
          <Route exact path="/" component={HomePage} />
          <Route exact path="/About" component={AboutPage} />
          <Route exact path="/Projects" component={ProjectsPage} />
          <Route exact path="/Gallery" component={GalleryPage} />
          <Route exact path="/Contact" component={ContactPage} />
        </div>
      </Router>
    );
  }
}

export default App;
