import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { GiKeyboard, GiBookshelf, GiAnnexation } from "react-icons/gi";

class AboutPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      skills: [{
        header: 'Programming Languages',
        icon: <GiKeyboard />,
        list: [
          'C#',
          'JavaScript',
          'TypeScript',
          'HTML',
          'CSS/Sass',
          'Python',
          'Linq',
          'Java',
          'C++',
          'C'
        ]
      }, {
        header: 'Frameworks/Libraries',
        icon: <GiBookshelf />,
        list: [
          '.NET Core',
          'ASP.NET',
          'AngularJS',
          'ReactJS',
          'React Native',
          'Redux'
        ]
      }, {
        header: 'Other',
        icon: <GiAnnexation />,
        list: [
          'Dynamics NAV',
          'Elasticsearch',
          'SQL (MSSQL, MySQL, SQLite)',
          'Node.js',
          'Git',
          'Scrum',
          'Agile',
          'Photoshop',
          'Illustrator',
          'Indesign'
        ]
      }]
    }
  }

  renderAboutText() {
    return (
      <div className="cut">
        <h1>About me...</h1>
        <p>
          I'm a hobby photographer and have a B.Sc. degree in computer scientist 
          from Reykjavik University (if you are reading this before 2nd of February 2019,
          you are lucky, you can see into the future!). My passion is full-stack web 
          development as I enjoy working with databases, APIs and front-end.
        </p>
        <p>
          My other passion is photographing animals as one never knows the result 
          or how they will react. As a photographer one needs to capture the once 
          in a lifetime moments that the animals will give in that photo session. 
          I'm experienced in photographing dogs and have been doing so since 2006.
        </p>
      </div>
    );
  }

  renderList(list) {
    const line = list.map((l) => {
      return (
        <li key={l}>{l}</li>
      );
    });

    return line;
  }

  renderSkills() {
    const {skills} = this.state;

    const skillStruct = skills.map((skill) => {
      return (
        <div
          className="skills"
          key={skill.header}
        >
          <div className="icon">{skill.icon}</div>
          <h3>{skill.header}</h3>
          <ul>
            {this.renderList(skill.list)}
          </ul>
        </div>
      );
    });

    return skillStruct;
  }

  render() {
    return (
      <div className="letter">
        <div className="a">A</div>
        <Scrollbars className="page">
          <div className="section about-me">
            {this.renderAboutText()}
          </div>
          <div className="stack section">
            {this.renderSkills()}
          </div>
        </Scrollbars>
      </div>
    )
  }
}
export default AboutPage;