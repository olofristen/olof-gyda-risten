import React, { Component } from 'react';

class HomePage extends Component {
  render() {
    return (
      <div className="page">
        <div className="r">R</div>
        <div className="home">
          <h2>Hello, I'm</h2>
          <div className="name-logo" />
        </div>
      </div>
    )
  }
}
export default HomePage;