import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import { FaMicrosoft, FaAngular, FaReact, FaSass, FaCss3Alt } from "react-icons/fa";
import 'react-vertical-timeline-component/style.min.css';

class ProjectsPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      projects: [{
        date: 'In progress',
        title: 'Online Resume',
        owner: 'Ólöf Gyða Risten',
        text: 'My online resume to practice my ReactJS and Sass skills along with making my projects available to all interested.',
        team: null,
        tags: [
          'ReactJS',
          'Sass'
        ],
        buttons: null
      }, {
        date: 'December 2017 - May 2018',
        title: 'Bílkaup - B.Sc. Final Project in Computer Science',
        owner: 'Sendiráðið vefstofa ehf',
        text: 'My B.Sc. final project in computer science at Reykjavik University. Bílkaup was made for Sendiráðið and is a web platform that enables carsales to display the cars they have for sale for anyone looking for a car the search is made with Elasticsearch and is therefore quick and easy to use.',
        team: [
          'Drífa Örvarsdóttir',
          'Bríet Konráðsdóttir',
          'Fanney Þóra Vilhjálmsdóttir',
          'Ólöf Gyða Risten Svansdóttir'
        ],
        tags: [
          'Angular',
          '.NET Core',
          'CSS',
          'MSSQL',
          'Elasticsearch'
        ],
        buttons: [{
          text: 'View Source Code',
          link: 'https://bitbucket.org/hrokering/bilkaup/'
        }, {
          text: 'View Documentation (ISL)',
          link: 'https://skemman.is/handle/1946/31154'
        }]
      }, {
        date: 'In progress',
        title: 'Webpage & Database',
        owner: 'Siberian Husky Club of Iceland',
        text: 'A webpage where all information about the club can be found along with a database of all Siberian Huskies in Iceland and their family tree.',
        team: null,
        tags: [
          'Angular',
          '.NET Core',
          'CSS',
          'SQLite (temporary)'
        ],
        buttons: [{
          text: 'View Source Code',
          link: 'https://bitbucket.org/olofristen/siberian-husky-database/'
        }]
      }],
      icons: [{
        icon: <FaMicrosoft />,
        name: 'MSSQL'
      }, {
        icon: <FaMicrosoft />,
        name: '.NET Core'
      }, {
        icon: <FaAngular />,
        name: 'Angular'
      }, {
        icon: <FaReact />,
        name: 'ReactJS'
      }, {
        icon: <FaSass />,
        name: 'Sass'
      }, {
        icon: <FaCss3Alt />,
        name: 'CSS'
      }]
    };
  }

  openInNewTab(url) {
    window.open(url, '_blank');
  }

  renderTeam(team) {
    if (team) {
      let listing = '';

      for (let i = 0; i < team.length; i++) {
        if (i === team.length - 1) {
          listing = listing + ` and ${team[i]}`;
        }

        if (i === 0) {
          listing = listing + `${team[i]}`;
        }

        if (i > 0 && i < team.length - 1) {
          listing = listing + `, ${team[i]}`;
        }
      }

      return (
        <div>
          <b>Team:</b> {listing}
        </div>
      );
    }

    return null;
  }

  renderButtons(buttons) {
    if (buttons) {
      const finalButton = buttons.map((b) => {
        return (
          <button 
            key={b.link}
            className="project-button" 
            onClick={() => this.openInNewTab(b.link)}
          >
              {b.text}
          </button>
        )
      });

      return finalButton;
    }

    return null;
  }

  renderTags(tags) {
    const { icons } = this.state;
    const tagLine = tags.map((tag) => {
      const ico = icons.find(icon => tag === icon.name); 
      if (ico) {
        return (
          <div
            key={tag}
            className="tag"
          >
            {ico.icon} {tag}
          </div>
        )
      }

      return (
        <div 
          key={tag}
          className="tag"
        >
          {tag}
        </div>
      )
    });

    return tagLine;
  }

  renderProjects() {
    const { projects } = this.state;

    const timeline = projects.map((project) => {
      const tags = project.tags;
      const buttons = project.buttons;
      const team = project.team;
      return (
        <VerticalTimelineElement
          key={project.title}
          className="vertical-timeline-element--work"
          date={project.date}
          iconStyle={{ background: 'rgb(33, 150, 243)', color: '#000' }}
        >
          <div className="tagholder">
            {this.renderTags(tags)}
          </div>
          <h3 className="vertical-timeline-element-title">{project.title}</h3>
          <h4 className="vertical-timeline-element-subtitle">{project.owner}</h4>
          <div className="project-text">
            {project.text}
          </div>
          <div className="team">
            {this.renderTeam(team)}
          </div>
          <div className="project-buttons">
            {this.renderButtons(buttons)}
          </div>
        </VerticalTimelineElement>
      )
    });

    return timeline;
  }

  render() {
    return (
      <div className="letter">
        <div className="p">P</div>
        <div className="pp">P</div>
        <Scrollbars className="page">
          <h1>Projects</h1>
          <p>A showcase of the projects I have been and still am working on.</p>
          <VerticalTimeline>
            {this.renderProjects()}
          </VerticalTimeline>
        </Scrollbars>
      </div>
    )
  }
}
export default ProjectsPage;