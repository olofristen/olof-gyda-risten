import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import Gallery from 'react-photo-gallery';

// Images
import aussie from '../Assets/images/gallery/aussie.jpg';
import bernhards from '../Assets/images/gallery/berhards.jpg';
import bernhardsPrt from '../Assets/images/gallery/bernhards_portrait.jpg';
import brella from '../Assets/images/gallery/brella.jpg';
import corgi from '../Assets/images/gallery/corgi.jpg';
import horseEye from '../Assets/images/gallery/horse_eye.jpg';
import horse from '../Assets/images/gallery/horse.jpg';
import knoxRock from '../Assets/images/gallery/knox_rock.jpg';
import legend from '../Assets/images/gallery/legend_woodss.jpg';
import lhasaLie from '../Assets/images/gallery/lhasa_lie.jpg';

class GalleryPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      columns: 0,
      componentWidth: 0,
      imagePerCol: [],
      images: [{
        src: aussie,
        width: 640,
        height: 960
      }, {
        src: bernhards,
        width: 2048,
        height: 1365
      }, {
        src: brella,
        width: 2048,
        height: 1365
      }, {
        src: bernhardsPrt,
        width: 640,
        height: 960
      }, {
        src: corgi,
        width: 640,
        height: 960
      }, {
        src: horseEye,
        width: 640,
        height: 960
      }, {
        src: horse,
        width: 640,
        height: 960
      }, {
        src: knoxRock,
        width: 640,
        height: 960
      }, {
        src: legend,
        width: 1866,
        height: 1296
      }, {
        src: lhasaLie,
        width: 2048,
        height: 1365
      }],
      gallery: []
    };
  }

  componentDidMount() {
    const { gallery } = this.state;
    if (gallery.length === 0) {
      this.setDimensions();
    }
  }

  setDimensions() {
    const { images } = this.state;

    const album = images.map((i) => {
      const tempObj = {
        src: null,
        height: null,
        width: null
      };

      tempObj.src = i;
      tempObj.height = i.height;
      tempObj.width = i.width;

      return tempObj;
    });

    this.setState({ gallery: album })
  }

  render() {
    const { images } = this.state;
    return (
      <Scrollbars className="page">
        <Gallery photos={images} />
      </Scrollbars>
    )
  }
}
export default GalleryPage;