import React, { Component } from 'react';
import { IoIosSnow } from "react-icons/io";

class MainBackground extends Component {

  constructor(props){
    super(props);
    this.state = {
      icon: <IoIosSnow />,
      large: 5,
      small: 7
    }
  }

  randomPercentage(counter, size) {
    if (counter < Math.floor(size/2)) {
      let rand = 100;
      while (rand > 50) {
        rand = Math.random() * 100;
      }
      return rand.toString() + '%';
    }

    if (counter >= Math.floor(size/2)) {
      let rand = 0;
      while (rand <= 50) {
        rand = Math.random() * 100;
      }
      return rand.toString() + '%';
    }
  }

  renderIcons(size) {
    const { icon } = this.state;
    const icons = [];

    for (let i = 0; i < size; i++) {
      const rand = this.randomPercentage(i, size);
      icons.push(
        <div key={i} className="icon" style={{left: rand}}>
          {icon}
        </div>
      )
    }

    return icons;
  }

  render() {
    const { large, small } = this.state;
    return (
      <div className="main-container">
        <div className="icons-large">
          {this.renderIcons(large)}
        </div>
        <div className="icons-small">
          {this.renderIcons(small)}
        </div>
      </div>
    );
  }
}
export default MainBackground;