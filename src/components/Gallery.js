import React, { Component } from 'react';
import { withSize } from 'react-sizeme';

// Images
import aussie from '../Assets/images/gallery/aussie.jpg';
import bernhards from '../Assets/images/gallery/berhards.jpg';
import bernhardsPrt from '../Assets/images/gallery/bernhards_portrait.jpg';
import brella from '../Assets/images/gallery/brella.jpg';
import corgi from '../Assets/images/gallery/corgi.jpg';
import horseEye from '../Assets/images/gallery/horse_eye.jpg';
import horse from '../Assets/images/gallery/horse.jpg';
import knoxRock from '../Assets/images/gallery/knox_rock.jpg';
import legend from '../Assets/images/gallery/legend_woodss.jpg';
import lhasaLie from '../Assets/images/gallery/lhasa_lie.jpg';

class Gallery extends Component {

  constructor(props){
    super(props);
    this.state = {
      columns: 0,
      componentWidth: 0,
      imagePerCol: [],
      images: [
        {
          link: aussie,
          orientation: 'portrait',
          show: false
        }, {
          link: bernhards,
          orientation: 'landscape',
          show: false
        }, {
          link: brella,
          orientation: 'landscape',
          show: false
        }, {
          link: bernhardsPrt,
          orientation: 'portrait',
          show: false
        }, {
          link: corgi,
          orientation: 'portrait',
          show: false
        }, {
          link: horseEye,
          orientation: 'portrait',
          show: false
        }, {
          link: horse,
          orientation: 'portrait',
          show: false
        }, {
          link: knoxRock,
          orientation: 'portrait',
          show: false
        }, {
          link: legend,
          orientation: 'landscape',
          show: false
        }, {
          link: lhasaLie,
          orientation: 'landscape',
          show: false
        }
      ]
    }
  }

  componentDidUpdate() {
    const { width } = this.props.size;
    const { componentWidth } = this.state;

    if (componentWidth !== width ) {
      this.setGrid();
    }
  }

  setGrid() {
    const { width } = this.props.size;

    this.setState({componentWidth: width});

    if (width < 600) {
      this.setState({ columns: 1 }, () => {
        this.setColPerImage()
      });
    }

    if (width >= 600 && width <= 768) {
      this.setState({ columns: 2 }, () => {
        this.setColPerImage()
      });
    }

    if (width >= 768 && width <= 992) {
      this.setState({ columns: 3 }, () => {
        this.setColPerImage()
      });
    }

    if (width >= 992 && width <= 1200) {
      this.setState({ columns: 4 }, () => {
        this.setColPerImage()
      });
    }

    if (width >= 1200) {
      this.setState({ columns: 5 }, () => {
        this.setColPerImage()
      });
    }
  }

  setColPerImage() {
    const { columns, images, imagePerCol } = this.state;

    if (columns) {
      let tempCol = columns;
      let leftover = images.length % columns;
      const equal = (images.length - leftover) / columns;

      const imgPerCol = [];
      while (leftover !== 0) {
        const num = equal + 1;
        imgPerCol.push(num);
        leftover = leftover - 1;
        tempCol = tempCol - 1;
      }

      while (tempCol !== 0) {
        imgPerCol.push(equal);
        tempCol = tempCol - 1;
      }

      if (JSON.stringify(imagePerCol) !== JSON.stringify(imgPerCol)) {
        this.setState({imagePerCol: imgPerCol});
      }
    }
  }

  renderImages(place) {
    const { images, imagePerCol } = this.state;
    const num = imagePerCol[place];
    const column = [];

    for (let i = 0; i < num; i++) {
      let img = images[Math.floor(Math.random()*images.length)];

      while (img.show) {
        img = images[Math.floor(Math.random()*images.length)];
      }
      img.show = true;

      const temp = (
        <div key={i}>
          <img src={img.link} alt="" />
        </div>
      );

      column.push(temp);
    }

    return column;
  }

  renderGallery() {
    const { columns } = this.state;

    const gallery = [];

    for (let i = 0; i < columns; i++) {
      const col = (
        <div className="column" key={i}>
          {this.renderImages(i)}
        </div>
      );
      gallery.push(col);
    }

    return gallery;
  }

  render() {
    return (
      <div className="gallery">
        {this.renderGallery()}
      </div>
    )
  }
}
export default withSize({ monitorHeight: true })(Gallery);