import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactHoverObserver from 'react-hover-observer';
import { GoHome, GoInfo, GoCode, GoEye, GoMail } from "react-icons/go";

class Header extends Component {
  constructor(props){
    super(props);
    this.state = {
      navigation: [
        {
          icon: <GoHome />,
          name: "Home",
          link: "/"
        }, {
          icon: <GoInfo />,
          name: "About",
          link: "/About"
        }, {
          icon: <GoCode />,
          name: "Projects",
          link: "/Projects"
        }, {
          icon: <GoEye />,
          name: "Gallery",
          link: "/Gallery"
        }, {
          icon: <GoMail />,
          name: "Contact",
          link: "/Contact"
        }
    
      ]
    }
  }

  renderName(icon) {
    return (
      <span className="link-text">{icon.name}</span>
    )
  }

  renderIcons() {
    const { navigation } = this.state;
    const nav = navigation.map((icon) => {
      const link = ({isHovering = false}) => (
        <Link to={icon.link} className={icon.name}>{isHovering ? this.renderName(icon) : icon.icon}</Link>
      );
      return (
        <li key={icon.name}>
          <ReactHoverObserver>
            {link}
          </ReactHoverObserver>
        </li>
      )
    });

    return nav;
  }

  render() {
    return (
      <header>
        <div className="logo" />
        <nav>
          <ul>
            {this.renderIcons()}
          </ul>
        </nav>
      </header>
    );
  }
}
export default Header;